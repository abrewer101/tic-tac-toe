# Tic-Tac-Toe Game

Welcome to this simple yet fun Tic-Tac-Toe game, designed to be played in any modern web browser. This project is built with HTML, CSS, and Vanilla JavaScript.


### Prerequisites

- A modern web browser like Chrome, Firefox, Safari, or Edge.
- Internet access (if you're playing the live version).

### Installing

To run this game locally:
1. Clone this repository to your local machine.
2. Navigate to the directory where you cloned the repository.
3. Open the `index.html` file in your web browser.

## How to Play

The game is a standard Tic-Tac-Toe where two players take turns marking spaces in a 3×3 grid with X or O. The player who succeeds in placing three of their marks in a horizontal, vertical, or diagonal row wins the game.

- To make a move, simply click on an empty square.
- To start a new game, click the "Reset Game" button.
